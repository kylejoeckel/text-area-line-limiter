### Title:      Text Area Limiter
### Author:     Kyle Joeckel
### Date:       11/02/18

#### Description:   
Limits line length and count based on the 
attributes `chars` and `lines` set in a `textarea` with 
class name of `line_control`. Once the limit has been reached or surpassed 
via paste event, only backspace or delete keys are valid.
On paste events it will provide an alert message 
with the lines that are over limit. You may also print the warning 
to the screen as shown in the example below.

#### Usage:      
``` html
<div>
<textarea class="line_control" id="something" lines="2" chars="8" style="resize: none;"></textarea>
<div class="theCount">
    Lines: <span id="something_linesUsed">0 </span>Chars: <span id="something_charsUsed">0</span>
</div>
<div style="font-size: 10px;"><span id="errors"></span></div>
</div>
```