/*
Title:      text_area_limiter
Author:     Kyle Joeckel
Date:       11/02/18

Descript:   Limits line length and count based on the 
            attributes `chars` and `lines` set in a textarea with 
            class name of `line_control`. oOnce the limit has been reached or surpassed 
            via paste event, only backspace or delete keys are valid.
            On paste events it will provide an alert message 
            with the lines that are over limit. You may also print the warning 
            to the screen as shown in the example below.

Usage:      <textarea class="line_control" id="something" lines="2" chars="8" style="resize: none;"></textarea>
            <div class="theCount">
                Lines: <span id="something_linesUsed">0 </span>Chars: <span id="something_charsUsed">0</span>
            </div>
            <div style="font-size: 10px;"><span id="errors"></span>
*/

$(document).ready(function(){
        var maxLength;
        var linecount;
        // FOCUS
        $('.line_control').focus(function (e) {
            if($(this).attr("chars")){
                var linesUsed = $('#'+e.target.id+'_linesUsed');
                var charsUsed = $('#'+e.target.id+'_charsUsed');
                var max_length = $('#'+e.target.id+'_max_length');
                var lines = $(this).attr("lines");
                var chars = $(this).attr("chars");
                var newLines = $(this).val().split("\n"); 
                var true_car = $(this)[0].selectionStart;
                var content = $(this).val().substr(0,true_car);
                linecount = 0;
                for(var i=0, len=content.length; i<len; i++){
                    if(content[i]==='\n'){
                        linecount++;
                    }
                }               
                maxLength = lines * chars;
                linesUsed.text(lines);
                max_length.text(maxLength);
                linesUsed.text(lines - newLines.length); 
                charsUsed.text(chars - newLines[linecount].length);
            }
        });
        $('.line_control').click(function (e) {
            if($(this).attr("chars")){
                var linesUsed = $('#'+e.target.id+'_linesUsed');
                var charsUsed = $('#'+e.target.id+'_charsUsed');
                var max_length = $('#'+e.target.id+'_max_length');
                var lines = $(this).attr("lines");
                var chars = $(this).attr("chars");
                var newLines = $(this).val().split("\n");
                var true_car = $(this)[0].selectionStart;
                var content = $(this).val().substr(0,true_car);
                linecount = 0;
                for(var i=0, len=content.length; i<len; i++){
                    if(content[i]==='\n'){
                        linecount++;
                    }
                }               
                maxLength = lines * chars;
                linesUsed.text(lines);
                max_length.text(maxLength);
                linesUsed.text(lines - newLines.length); 
                charsUsed.text(chars - newLines[linecount].length);
            }
        });
        // KEYUP EVENTS
        $('.line_control').keyup(function (e) {
            if($(this).attr("chars")){
                var linesUsed = $('#'+e.target.id+'_linesUsed');
                var charsUsed = $('#'+e.target.id+'_charsUsed');
                var errorreading = $('#'+e.target.id+'_errors');
                var max_length = $('#'+e.target.id+'_max_length');
                var cur_line = $('#'+e.target.id+'_crline');
                var lines = $(this).attr("lines");
                var chars = $(this).attr("chars");
                var totalChars = 0;
                var newLines = $(this).val().split("\n");  
                totalChars = getTotalChar(newLines);
                var true_car = $(this)[0].selectionStart;
                var content = $(this).val().substr(0,true_car);
                linecount = 0;
                for(var i=0, len=content.length; i<len; i++){
                    if(content[i]==='\n'){
                        linecount++;
                    }
                }
                carret_pos = $(this)[0].selectionStart -(newLines.length - 1);
                cur_line.text(linecount+1);
                if((linecount+1)>=lines){
                    cur_line.css('color', 'red');
                }else{
                    cur_line.css('color', 'red');
                }
                maxLength = lines * chars;
                linesUsed.text(lines - newLines.length);
                max_length.text(maxLength-totalChars);
                charsUsed.text(chars - newLines[linecount].length);
                if((chars - newLines[linecount].length)<=0){
                    charsUsed.text(0);
                    charsUsed.css('color', 'red');
                }else{          
                    charsUsed.css('color', '');
                }
                if(lines - newLines.length <= 0){
                    linesUsed.text(0);
                    linesUsed.css('color', 'red');
                }else{
                    linesUsed.css('color', '');
                }
                if(totalChars >= maxLength ){
                    max_length.css('color', 'red');
                }else{
                    max_length.css('color', '');
                }
            }
        });
        //HANDLES AND KEYDOWN EVENTS
        $('.line_control').keydown(function (e) {
            if($(this).attr("chars")){
                var linesUsed = $('#'+e.target.id+'_linesUsed');
                var charsUsed = $('#'+e.target.id+'_charsUsed');
                var errorreading = $('#'+e.target.id+'_errors');
                var max_length = $('#'+e.target.id+'_max_length');
                var cur_line = $('#'+e.target.id+'_crline');
                var totalChars = 0;
                var newLines = $(this).val().split("\n");  
                totalChars = getTotalChar(newLines);
                var lines = $(this).attr("lines");
                var chars = $(this).attr("chars");
                maxLength = lines * chars;
                if(e.keyCode == 13 && newLines.length >= lines){
                    alert("You've reached the maximum number of lines allowed");
                    return false;
                }
                if (newLines.length > lines && e.keyCode !== 8 && e.keyCode !== 46 && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40) {
                    return false;
                }
                if(newLines[linecount]){
                    if (e.keyCode !== 13 && e.keyCode !== 8 && e.keyCode !== 46 && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40 && newLines[linecount].length >= chars) {
                        return false;
                    }
                }
                if(totalChars >= maxLength  && e.keyCode !== 8 && e.keyCode !== 46 && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40){
                    return false;
                }
            }
        });
        // HANDLES PASTE EVENTS
        $('.line_control').on('paste', function (e) {
            if($(this).attr("chars")){
                var linesUsed = $('#'+e.target.id+'_linesUsed');
                var charsUsed = $('#'+e.target.id+'_charsUsed');
                var errorreading = $('#'+e.target.id+'_errors');
                var max_length = $('#'+e.target.id+'_max_length');
                var cur_line = $('#'+e.target.id+'_crline');
                var $el = $(this);
                var lines = $el.attr("lines");
                var chars = $el.attr("chars");
                var errors = [];
                setTimeout( (e) => {
                    var newLines = $el.val().split("\n");
                    console.log(newLines);
                    linesUsed.text(newLines.length);
                    charsUsed.text(newLines[newLines.length - 1].length + 1);
                    for (var i = 0, len = newLines.length; i < len; i++) {
                        if (newLines[i].length >= chars) {
                            let line = i + 1;
                            let count = newLines[i].length;
                            errors.push({
                                'line': line,
                                'count': count
                            })
                        }
                    }
                    if (errors.length > 0) {
                        var html = '<p>Errors:</p>';
                        var alertMessage = "Warning!\n\nYour pasted content has exceeded the line limitations. Please review the following:\n\n"
                        for (var i = 0, len = errors.length; i < len; i++) {
                            html = html + '<span>Line: ' + errors[i]['line'] + '</span></br><span>Count: ' + errors[i]['count'] + '</span></br>'
                            alertMessage = alertMessage + 'Line: ' + errors[i]['line'] + ' Over: ' + (errors[i]['count'] - chars) + ' Count: ' + errors[i]['count'] + '\n';
                        }
                        alert(alertMessage);
                        errorreading.html(html);
                    }
                    console.log(errors);
                    if (newLines.length >= lines) {
                        linesUsed.css('color', 'red');
                        return false;
                    } else {
                        linesUsed.css('color', '');
                    }
                    if (newLines[newLines.length - 1].length >= chars) {
                        charsUsed.css('color', 'red');
                        return false;
                    } else {
                        charsUsed.css('color', '');
                    }

                }, 100);
            }
        });
        function getTotalChar (newLines){
            let totalChars = 0;
            newLines.forEach(element => {
                totalChars = totalChars + element.length;
            });
            return totalChars;
        };
    });
